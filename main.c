#include <stdio.h>
#include "liste.h"

int main()
{
  Liste l;
  int rang;

  l = liste_initialiser();
  liste_afficher(l);
  l = liste_teteinserer(l, 6);
  liste_afficher(l);
  l = liste_queue_inserer(l, 9);
  liste_afficher(l);
  l = liste_queue_supprimer(l);
  liste_afficher(l);

  if (liste_rechercher(l, 6, &rang))
      printf("Élément 6 trouvé en rang %d\n", rang);
      
  /* Ménage */

  liste_supprimer_liste(l);
  return 0;
}
